from AppStock_DB import *
from AppStock_funciones import *

nombreBD = "AppStock.db"
AppStock = BaseDatos(nombreBD)

AppStock.ConectarBase()

############################### menu principal
menu = 9
permiso=MenuPrincipal()

while menu != 0:
    menu_articulos = 9
    menu_usuarios = 9
    menu_admin = 9
    menu = MenuFunciones()
    match menu:
        case 1: ############################### MENU ARTICULOS
            BorradoPantalla()
            while menu_articulos !=0:
                menu_articulos = MenuArticulos()
                match menu_articulos:
                    case 1: 
                        BorradoPantalla()
                        BuscarArticuloSKU()
                        TeclaContinuar()
                    case 2:
                        BorradoPantalla()
                        BuscarArticuloNombre()
                        TeclaContinuar()
                    case 3:
                        BorradoPantalla()
                        BuscarArticuloTodos()
                        TeclaContinuar()
                    case 0:
                        menu_articulos = 0
        case 2: ############################### MENU USUARIOS
            BorradoPantalla()
            while menu_usuarios != 0:
                menu_usuarios = MenuUsuarios()
                match menu_usuarios:
                    case 1:
                        BorradoPantalla()
                        BuscarUserID()
                        TeclaContinuar()
                    case 2:
                        BorradoPantalla()
                        BuscarUserNombre()
                        TeclaContinuar()
                    case 3:
                        BorradoPantalla()
                        BuscarUserTodos()
                        TeclaContinuar()
                    case 0:
                        menu_usuarios = 0
        case 3: ############################### MENU ADMINISTRATIVAS
            BorradoPantalla()
            if permiso == True:
                while menu_admin != 0:
                    menu_admin = MenuAdmin()
                    match menu_admin:
                        case 1:
                            BorradoPantalla()
                            ApiEconomico()
                            TeclaContinuar()
                        case 2:
                            BorradoPantalla()
                            IngresarArticulo()
                            TeclaContinuar()
                        case 3:
                            BorradoPantalla()
                            ActualizarArticuloSKU()
                            TeclaContinuar()
                        case 4:
                            BorradoPantalla()
                            BorraArticulo()
                            TeclaContinuar()
                        case 5:
                            BorradoPantalla()
                            IngresarUser()
                            TeclaContinuar()
                        case 6:
                            BorradoPantalla()
                            ActualizarUserID()
                            TeclaContinuar()
                        case 7:
                            BorradoPantalla()
                            BorraUser()
                            TeclaContinuar()
                        case 0:
                            menu_admin = 0
            else:
                print("\n\x1b[1;31m Ud. no tienen los permisos necesarios para esta seccion\x1b[1;37m")
                menu_admin = 0
                TeclaContinuar()
        case 0: ############################### CERRAR BASE DE DATOS
            BorradoPantalla()
            AppStock.cerrarBase()
            print(f"""\x1b[1;33m     
      ___                _____ __             __  
     /   |  ____  ____  / ___// /_____  _____/ /__
    / /| | / __ \/ __ \ \__ \/ __/ __ \/ ___/ //_/
   / ___ |/ /_/ / /_/ /___/ / /_/ /_/ / /__/ ,<   
  /_/  |_/ .___/ .___/_____/\__/\____/\___/_/|_|  
--------/_/---/_/---------------------------------
                  \x1b[1;32mBase de datos CERRADA\x1b[1;37m""")
            TeclaContinuar()           

# Crear funcion para cambiar al usuario conectado.
# Crear validacion para ingresar letras no numeros y signos.
# Crear validacion para evitar que no se ingrese informacion (espacion en blanco)
# Colocar los TRY ... EXCEPT donde corresponda para evitar que se caiga el programa