import sqlite3

class BaseDatos():
    def __init__(self, nombreBase):
        self.nombrebase = nombreBase
        self.conexion = None
        self.cursor = None
  
    def ConectarBase(self):
        self.conexion = sqlite3.connect("AppStock.db") 
        self.cursor = self.conexion.cursor()

    def ConsultarArticuloSKU(self, sku):
        articulo = self.cursor.execute("SELECT * FROM ARTICULOS WHERE SKU = ?", (sku,))
        return articulo 

    def ConsultarArticuloNombre(self, nombre):
        articulo = self.cursor.execute("SELECT * FROM ARTICULOS WHERE NOM_ART LIKE ?", (nombre,))
        return articulo
    
    def ConsultarArticuloTodos(self):
        articulo = self.cursor.execute("SELECT * FROM ARTICULOS")
        return articulo             

    def ConsultarUserID(self, rut):
        usuario = self.cursor.execute("SELECT RUT||'-'|| DV, NOM_USER, APEL_USER, CATEGORIA FROM USUARIOS WHERE RUT = ?", (rut,))
        return usuario 
    
    def ConsultarUserNombre(self, nombre):
        usuario = self.cursor.execute("SELECT RUT||'-'|| DV, NOM_USER, APEL_USER, CATEGORIA FROM USUARIOS WHERE NOM_USER LIKE ?", (nombre,))
        return usuario
    
    def ConsultarUserTodos(self):
        usuario = self.cursor.execute("SELECT RUT||'-'|| DV, NOM_USER, APEL_USER, CATEGORIA FROM USUARIOS")
        return usuario  

    def NuevoUsuario(self, rut,dv,nom_user,apel_user,categoria,clave):
        self.cursor.execute("INSERT INTO USUARIOS (RUT,DV,NOM_USER,APEL_USER,CATEGORIA,CLAVE) values (?,?,?,?,?,?)", (rut,dv,nom_user,apel_user,categoria,clave))
        self.conexion.commit()
        print("\n\n\x1b[1;32m  Usuario ingresado correctamente.\x1b[1;37m")

    def NuevoArticulo(self, nom_art,stock,precio_venta,precio_costo,descripcion):
        self.cursor.execute("INSERT INTO ARTICULOS (NOM_ART,STOCK,PRECIO_VENTA,PRECIO_COSTO,DESCRIPCION) values (?,?,?,?,?)", (nom_art,stock,precio_venta,precio_costo,descripcion))
        self.conexion.commit()
        print("\n\n\x1b[1;32m  Articulo ingresado correctamente.\x1b[1;37m")

    def BorrarUsuario(self, rut):
        self.cursor.execute("DELETE FROM USUARIOS WHERE RUT = ?", (rut,))
        self.conexion.commit()
        print("\n\n\x1b[1;32m  Usuario eliminado correctamente.\x1b[1;37m")

    def BorrarArticulo(self, sku):
        self.cursor.execute("DELETE FROM ARTICULOS WHERE SKU = ?", (sku,))
        self.conexion.commit()
        print("\n\n\x1b[1;32m  Artículo eliminado correctamente.\x1b[1;37m")
        
    def cerrarBase(self):
        self.cursor.close()
        self.conexion.close()

    def Login(self,rut):
        usuario = self.cursor.execute("SELECT RUT, CLAVE, CATEGORIA FROM USUARIOS WHERE RUT = ?", (rut,))
        return usuario

    def ActualizarArticulo(self, sku, nuevo_nom_art, nuevo_stock, nuevo_precio_venta, nuevo_precio_costo, nueva_descripcion):
        self.cursor.execute("UPDATE ARTICULOS SET NOM_ART=?, STOCK=?, PRECIO_VENTA=?, PRECIO_COSTO=?, DESCRIPCION=? WHERE SKU = ?", (nuevo_nom_art,nuevo_stock,nuevo_precio_venta,nuevo_precio_costo,nueva_descripcion,sku,))
        self.conexion.commit()
        print(f"\n\n\x1b[1;32m  Artículo actualizado correctamente.\x1b[1;37m")
        
    def ActualizarUsuario(self, rut, nom_user,apel_user,categoria,clave):
        self.cursor.execute("UPDATE USUARIOS SET NOM_USER=?, APEL_USER=?, CATEGORIA=?, CLAVE=? WHERE RUT = ?", (nom_user,apel_user,categoria,clave,rut,))
        self.conexion.commit()
        print(f"\n\n\x1b[1;32m  Artículo actualizado correctamente.\x1b[1;37m")

# class Usuarios(BaseDatos):
#     def __init__(self, nombreBase):
#         super().__init__(nombreBase)
             
# class Articulo(BaseDatos):
#     def __init__(self, nombreBase,sku,nombre_articulo,stock,precio_venta,precio_costo,descripcion):
#         super().__init__(nombreBase)