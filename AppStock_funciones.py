import subprocess
def instalar_libreria(nombre_libreria):
    try:
        # Utiliza subprocess para ejecutar el comando de instalación
        subprocess.check_call(["pip", "install", nombre_libreria])
        print(f"\x1b[1;32mLa librería {nombre_libreria} instalada.\x1b[1;37m")
    except subprocess.CalledProcessError as e:
        print(f"\x1b[1;31mError al instalar la librería {nombre_libreria}: {e}\x1b[1;37m")
    except Exception as e:
        print(f"\x1b[1;31mError inesperado: {e}\x1b[1;37m")

requests_pip = "requests"
tabulate_pip = "tabulate"
pandas_pip = "pandas"
stdiomask_pip = "stdiomask"
instalar_libreria(requests_pip)
instalar_libreria(tabulate_pip)
instalar_libreria(pandas_pip)
instalar_libreria(stdiomask_pip)

from AppStock_DB import *
from tabulate import tabulate #https://python-para-impacientes.blogspot.com/2017/01/tablas-con-estilo-con-tabulate.html
import pandas as pd
import time,os, msvcrt
from datetime import date
from datetime import datetime


nombreBD = "AppStock.db"
AppStock = BaseDatos(nombreBD)
AppStock.ConectarBase()

############################### funciones para la base de datos

def LoginUser():
    import stdiomask # Cambiar la contrasena por *
    print("Accediendo al sistema\n")
    while True:
        user_id = input("\x1b[1;32m-> Ingrese ID Usuario: \x1b[1;37m")
        #clave = input("\nIngrese contraseña: ")
        clave = stdiomask.getpass("\x1b[1;32m-> Ingrese Contraseña: \x1b[1;37m")
        user_encontrado = AppStock.Login(user_id)
        try:
            if user_encontrado is not None:
                datos = pd.DataFrame(user_encontrado, columns=['RUT', 'CONTRASENA', 'CATEGORIA'])
                rut = int(datos['RUT'].values[0])
                contrasena = str(datos['CONTRASENA'].values[0])
                categoria = str(datos['CATEGORIA'].values[0])
                if contrasena == clave:
                    print("\n\x1b[1;37m   ¡Hola, bienvenido al sistema!\n")
                    if categoria == 'ADMIN':
                        print("-> Su rol actual es: \x1b[1;33mAdministrador\n")
                        BarraCarga()
                        print()
                        return  True
                    else:
                        print("-> Su rol actual es: \x1b[1;33mVendedor\n")
                        BarraCarga()
                        print()
                        return False
                else:
                    print("\n\x1b[1;31mContraseña incorrecta. \x1b[1;37mVuelva a intentar!\n")     
            else:
                print("\n\x1b[1;31mUsuario no encontrado\x1b[1;37m")
        except:
            print("\n\x1b[1;31mUsuario no encontrado, \x1b[1;37mtrate nuevamente.\n")

def BuscarArticuloSKU():
    print(f"\n \x1b[1;33mBUSCAR ARTICULO POR SKU\x1b[1;37m\n")
    print(f" - Ingrese SKU a buscar")
    articulo_buscar=validacionNum()
    articulo_SKU= AppStock.ConsultarArticuloSKU(articulo_buscar)
    datos=pd.DataFrame(articulo_SKU)
    headers=['SKU','ARTICULO','STOCK','PRECIO VENTA', 'PRECIO COSTO','DESCIPCION']
    print()
    print(tabulate(datos,showindex='never',numalign='center',stralign='left',headers=headers,tablefmt='mixed_outline'))
    return articulo_buscar

def BuscarArticuloNombre():
    print(f"\n \x1b[1;33mBUSCAR ARTICULO POR NOMBRE\x1b[1;37m\n")
    print("\n - Ingrese Nombre o Inicial del articulo")
    articulo_buscar=validacionTXT().upper()
    articulo_nombre=AppStock.ConsultarArticuloNombre(f"{articulo_buscar}%")
    datos=pd.DataFrame(articulo_nombre)
    headers=['SKU','ARTICULO','STOCK','PRECIO VENTA', 'PRECIO COSTO','DESCIPCION']
    print()
    print(tabulate(datos,showindex='never',numalign='center',stralign='left',headers=headers,tablefmt='mixed_outline'))

def BuscarArticuloTodos():
    articulo_todos= AppStock.ConsultarArticuloTodos()
    datos=pd.DataFrame(articulo_todos)
    headers=['SKU','ARTICULO','STOCK','PRECIO VENTA', 'PRECIO COSTO','DESCIPCION']
    print()
    print(tabulate(datos,showindex='never',numalign='center',stralign='left',headers=headers,tablefmt='mixed_outline'))

def IngresarArticulo(): 
    print(f"\n \x1b[1;33mNUEVO ARTICULO\x1b[1;37m\n")
    print(" - Nombre ")
    nom_art = input(f"\x1b[1;32mIngrese valor:\x1b[1;37m ").upper()
    print("\n - Stock")
    stock = validacionNum()
    print("\n - Precio de Venta")
    precio_venta = validacionNum()
    print("\n - Precio de Costo")
    precio_costo = validacionNum()
    descripcion = input(f"\n\x1b[1;32m - Ingrese descripcion: \x1b[1;37m").upper()
    AppStock.NuevoArticulo(nom_art,stock,precio_venta, precio_costo, descripcion) 

def BorraArticulo():
    print(f"\n \x1b[1;33mBORRAR ARTICULO\x1b[1;37m\n")
    print(f" - Ingrese SKU a eliminar")
    articulo_buscar = validacionNum()
    articulo_SKU= AppStock.ConsultarArticuloSKU(articulo_buscar)
    datos=pd.DataFrame(articulo_SKU)
    headers=['SKU','ARTICULO','STOCK','PRECIO VENTA', 'PRECIO COSTO','DESCIPCION']
    print()
    print(tabulate(datos,showindex='never',numalign='center',stralign='left',headers=headers,tablefmt='mixed_outline'))
    AppStock.BorrarArticulo(articulo_buscar)

def BuscarUserID():
    print(f"\n \x1b[1;33mBUSCAR USUARIO POR ID\x1b[1;37m\n")
    print(f" - Ingrese ID a buscar")
    user_id=validacionNum()
    user_encontrado= AppStock.ConsultarUserID(user_id)
    datos=pd.DataFrame(user_encontrado)
    headers=['RUT','NOMBRE','APELLIDO', 'CATEGORIA']
    print()
    print(tabulate(datos,showindex='never',numalign='left',stralign='left',headers=headers,tablefmt='mixed_outline'))
    return user_id

def BuscarUserNombre():
    print(f"\n \x1b[1;33mBUSCAR USUARIO POR NOMBRE\x1b[1;37m\n")
    print("\n - Ingrese Nombre o Inicial del Usuario")
    user_buscar=validacionTXT().upper()
    user_nombre=AppStock.ConsultarUserNombre(f"{user_buscar}%")
    datos=pd.DataFrame(user_nombre)
    headers=['RUT','NOMBRE','APELLIDO', 'CATEGORIA']
    print()
    print(tabulate(datos,showindex='never',numalign='left',stralign='left',headers=headers,tablefmt='mixed_outline'))

def BuscarUserTodos():
    user_nombre=AppStock.ConsultarUserTodos()
    datos=pd.DataFrame(user_nombre)
    headers=['RUT','NOMBRE','APELLIDO', 'CATEGORIA']
    print()
    print(tabulate(datos,showindex='never',numalign='left',stralign='left',headers=headers,tablefmt='mixed_outline'))

def IngresarUser(): 
    print(f"\n \x1b[1;33mNUEVO USUARIO\x1b[1;37m\n")
    print(f" - RUT")
    rut=validacionNum()
    print(f"\n - Digito Verificador")
    dv=validacionNum() # CREAR FUNCION PARA QUE SE CALCULE SOLO
    print(f"\n - Nombre")
    nom_user=validacionTXT().upper()
    print(f"\n - Apellido")    
    apel_user=validacionTXT().upper()
    print(f"\n - Categoria (ADMIN/VENDEDOR)") 
    categoria=validacionTXT().upper()
    print(f"\n - Clave Numerica")
    clave=validacionNum()
    AppStock.NuevoUsuario(rut,dv,nom_user,apel_user,categoria,clave) 

def BorraUser():
    print(f"\n \x1b[1;33mBORRAR USUARIO\x1b[1;37m\n")
    print(f" - Ingrese ID a eliminar")
    user_id=validacionNum()
    user_encontrado= AppStock.ConsultarUserID(user_id)
    datos=pd.DataFrame(user_encontrado)
    headers=['RUT','NOMBRE','APELLIDO', 'CATEGORIA']
    print()
    print(tabulate(datos,showindex='never',numalign='left',stralign='left',headers=headers,tablefmt='mixed_outline'))
    #print("\n Usuario a eliminar")
    AppStock.BorrarUsuario(user_id)

def ActualizarArticuloSKU(): ################# AL NO ENCONTRAR ARTICULO DETENER ACTUALIZACION
    print(f"\n \x1b[1;33mACTUALIZAR ARTICULO\x1b[1;37m\n")
    sku = BuscarArticuloSKU()
    print("\n - Ingrese Nuevos Parametros\n") 
    print(" - Nombre ")
    nom_art = input(f"\x1b[1;32mIngrese valor:\x1b[1;37m ").upper()
    print(f"\n - Stock")
    stock = validacionNum()
    print("\n - Precio de Venta")
    precio_venta = validacionNum()
    print("\n - Precio de Costo")
    precio_costo = validacionNum()
    descripcion = input(f"\n\x1b[1;32m - Ingrese Descripcion:\x1b[1;37m ").upper()
    AppStock.ActualizarArticulo(sku,nom_art,stock,precio_venta, precio_costo, descripcion) 

def ActualizarUserID():   ################# AL NO ENCONTRAR USUARIO DETENER ACTUALIZACION
    print("\n \x1b[1;33mACTUALIZAR USUARIO\x1b[1;37m\n")
    rut=BuscarUserID()
    print("\n - Ingrese Nuevos Parametros\n")
    print("\n - Nombre")
    nom_user=validacionTXT().upper()  
    print("\n - Apellido")    
    apel_user=validacionTXT().upper()
    print("\n - Categoria (ADMIN/VENDEDOR)")
    categoria=validacionTXT().upper()
    print("\n - Clave")
    clave=validacionNum()
    AppStock.ActualizarUsuario(rut,nom_user,apel_user,categoria,clave) 

############################### funciones de la app

def BorradoPantalla():
    time.sleep(0.5) 
    os.system('cls')

def TeclaContinuar():
    print("\n\x1b[1;37m     ¡Presione una tecla para continuar!\n")
    msvcrt.getch()

def validacionNum():
    while True:
        try :
            num = int(input("\x1b[1;32mIngrese valor: \x1b[1;37m"))
            return num
        except ValueError as errorTXT:
            print (f"\n\x1b[1;31m¡Por favor, no ingrese letras ni signos!\x1b[1;37m\n")

def validacionTXT():
    while True:
        char = str(input("\x1b[1;32mIngrese valor: \x1b[1;37m"))
        if not char.isspace() and char.isalpha():
            return char
        else:
            print("\n\x1b[1;31m¡Por favor, no ingrese números, espacio ni vacío!\x1b[1;37m\n")

def BarraCarga():
    #BorradoPantalla()
    cadena = ' ' * 50
    caracter='|'
    b=0
    for i in range(100):
        if (i%2==0):
            x=list(cadena)
            x[b]=caracter
            cadena="".join(x)
            b+=1
        print(f"\x1b[1;37m AppStock - Cargando Base de Datos . . . \x1b[1;31m{i+1}% {cadena} ", end='\r')
        time.sleep(0.03)

def MenuPrincipal():
    BorradoPantalla()
    time.sleep(0.5) 
    now = datetime.now()
    formato = now.strftime('%d/%m/%Y, %H:%M:%S')
    print(f"""\x1b[1;33m
          
      ___                _____ __             __  
     /   |  ____  ____  / ___// /_____  _____/ /__
    / /| | / __ \/ __ \ \__ \/ __/ __ \/ ___/ //_/
   / ___ |/ /_/ / /_/ /___/ / /_/ /_/ / /__/ ,<   
  /_/  |_/ .___/ .___/_____/\__/\____/\___/_/|_|  
--------/_/---/_/---------------------------------\x1b[1;37m
                              {formato}""")
    Permiso=LoginUser()
    TeclaContinuar()
    return Permiso

def MenuFunciones():
    BorradoPantalla()
    print("""
       \x1b[1;33mA P P  S T O C K\x1b[1;37m      
    ----------------------
    | \x1b[1;35m1.- \x1b[1;37mARTICULOS      |
    | \x1b[1;35m2.- \x1b[1;37mUSUARIOS       |
    | \x1b[1;35m3.- \x1b[1;37mADMINISTRACION |
    |                    |
    | \x1b[1;31m0.- \x1b[1;31mSALIR\x1b[1;37m          | 
    ----------------------                      
    """)
    menu=validacionNum()
    return menu

def MenuArticulos():
    BorradoPantalla()
    print("""
        \x1b[1;34mA R T I C U L O S\x1b[1;37m   
    -------------------------
    | \x1b[1;35m1.- \x1b[1;37mBUSCAR POR SKU    |
    | \x1b[1;35m2.- \x1b[1;37mBUSCAR POR NOMBRE |
    | \x1b[1;35m3.- \x1b[1;37mMOSTRAR TODOS     |
    |                       |
    | \x1b[1;31m0.- \x1b[1;31mSALIR\x1b[1;37m             |
    -------------------------
    """)
    menu = validacionNum()
    return menu

def MenuUsuarios():
    BorradoPantalla()
    print("""
         \x1b[1;32mU S U A R I O S \x1b[1;37m    
    -------------------------
    | \x1b[1;35m1.- \x1b[1;37mBUSCAR POR ID     |
    | \x1b[1;35m2.- \x1b[1;37mBUSCAR POR NOMBRE |
    | \x1b[1;35m3.- \x1b[1;37mMOSTRAR TODOS     |
    |                       |
    | \x1b[1;31m0.- \x1b[1;31mSALIR\x1b[1;37m             |
    -------------------------
    """)
    menu = validacionNum()
    return menu

def MenuAdmin():
    BorradoPantalla()
    print("""
            \x1b[1;33mADMINISTRACION\x1b[1;37m          
    ---------------------------------
    | \x1b[1;35m1.- \x1b[1;37mINDICES ECONOMICOS        |
    |                               |
    | \x1b[1;35m2.- \x1b[1;34mARTICULO - INGRESAR NUEVO\x1b[1;37m |
    | \x1b[1;35m3.- \x1b[1;34mARTICULO - ACTUALIZAR\x1b[1;37m     |
    | \x1b[1;35m4.- \x1b[1;34mARTICULO - ELIMINAR\x1b[1;37m       |
    |                               |
    | \x1b[1;35m5.- \x1b[1;32mUSUARIO  - INGRESAR NUEVO |
    | \x1b[1;35m6.- \x1b[1;32mUSUARIO  - ACTUALIZAR     |
    | \x1b[1;35m7.- \x1b[1;32mUSUARIO  - ELIMINAR       |
    |                               |
    | \x1b[1;31m0.- \x1b[1;31mSALIR\x1b[1;37m                     |
    ---------------------------------
    """)
    menu = validacionNum()
    return menu

############################### CONEXION CON API

def ApiEconomico():
    import requests,json
    url = "https://mindicador.cl/api"    
    response = requests.get(url)

    try:
        if response.status_code == 200:
            data = json.loads(response.content)
            autor = data['autor']
            fecha = data['fecha']
            uf= data['uf']['valor']
            dolar = data['dolar']['valor']
            euro = data['euro']['valor']
            print(f"""\x1b[1;37m
    Según \x1b[1;33m{autor} \x1b[1;37mlos valores para hoy \x1b[1;33m{fecha}\x1b[1;37m
        
        - Dolar : \x1b[1;33m{dolar}\x1b[1;37m
        - Euro  : \x1b[1;33m{euro}\x1b[1;37m
        - UF    : \x1b[1;33m{uf}\x1b[1;37m""")
    except:
        print(f"\x1b[1;33mNo se pudo obtener la información.\x1b[1;37m")

# def digito_verificador(rut): #https://gist.github.com/rbonvall/464824
#     from itertools import cycle
#     reversed_digits = map(int, reversed(str(rut)))
#     factors = cycle(range(2, 8))
#     s = sum(d * f for d, f in zip(reversed_digits, factors))
#     return (-s) % 11 if (-s) % 11 < 10 else 'K'